# language: pt
Funcionalidade: Retirada presencial e pagamento em dinheiro

  Cenario: Retirada presencial e pagamento em dinheiro

    Dado um cliente que fechou um pedido
    Quando seleciona as formas de entrega
    Então Pagar-na-retirada deveria ser selecionável

    